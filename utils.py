from collections import namedtuple
Track = namedtuple("Track", "artist track url")
LocalTrack = namedtuple('LocalTrack', Track._fields + ('file',))

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def download_tracks_page(chrome_driver_path, url):
    from selenium import webdriver
    from time import sleep
    import datetime

    driver = webdriver.Chrome(chrome_driver_path)
    driver.get(url)

    # scroll page down
    prev_source = driver.page_source
    time = datetime.datetime.now()

    scroll_num = 0
    while True:
        scroll_num = scroll_num + 1
        print("Scroll down", scroll_num)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        if prev_source != driver.page_source:
            prev_source = driver.page_source
            time = datetime.datetime.now()
        else:
            if (datetime.datetime.now() - time).total_seconds() > 3:
                break

        sleep(1)

    print("Page loaded")
    page = driver.page_source
    driver.quit()
    return page

def parse_page(page):
    import xml.etree.ElementTree as ET

    out_tracks = list()

    import lxml.html
    tree = lxml.html.fromstring(page)
    items = tree.cssselect('li.soundList__item')

    for div in reversed(items):
        try:
            title = div.cssselect('a.soundTitle__title')

            t = Track(
                div.cssselect('span.soundTitle__usernameText')[0].text,
                title[0].xpath('./span')[0].text,
                'https://soundcloud.com' + title[0].attrib['href']
            )

            out_tracks.append(t)
        except Exception as e:
            print("Parsing failed:", e,  ET.tostring(div))
    return out_tracks

def download_to_file(url, file_name):
    from requests import get
    try:
        response = get(url)
        with open(file_name, "wb") as file:
            file.write(response.content)
    except Exception as e:
        print('Download url to file failed', e)
        raise e

def download(url):
    from requests import get

    response = get(url)
    response.raise_for_status()
    return response.content

def fix_file_name(file_name):
    import string
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in file_name if c in valid_chars)

def request_track_art_urls(song_url):
    try:
        return request_track_art_urls_1(song_url)
    except Exception as e:
        print(bcolors.WARNING, e, ", trying another server...", bcolors.ENDC)
        return request_track_art_urls_2(song_url)

def request_track_art_urls_2(track_url):
    from requests_html import HTMLSession

    BASE_URL = 'https://sclouddownloader.net/'
    DOWNLOAD_URL = 'https://sclouddownloader.net/download-sound-track'
    print("Requesting from", BASE_URL)

    session = HTMLSession()
    page = session.get(BASE_URL)

    try:
        token = page.html.find('input[name=csrfmiddlewaretoken]', first=True).attrs['value']
    except Exception as e:
        print("Error while getting csrf:", e)
        raise e

    payload = {'csrfmiddlewaretoken': token, 'sound-url': track_url}
    session.headers['Referer'] = BASE_URL
    page = session.post(DOWNLOAD_URL, data=payload)

    try:
        url = page.html.find('a[href][download]', first=True).attrs['href']
        art_url = page.html.find('img[id=thumbnail]', first=True).attrs['src']
    except Exception as e:
        print("Culd not get download link from :", BASE_URL, e)
        raise e
    return (url, art_url)

def request_track_art_urls_1(track_url):
    from requests_html import HTMLSession
    BASE_URL = 'https://soundcloudtomp3.app/'
    DOWNLOAD_URL = 'https://soundcloudtomp3.app/download'

    print("Requesting from", BASE_URL)
    session = HTMLSession()
    page = session.get(BASE_URL)

    payload = {'formurl': track_url}
    session.headers['Referer'] = BASE_URL
    page = session.post(DOWNLOAD_URL, data=payload)

    try:
        url = page.html.find('a[title="Righ Click -> Save Link As"]', first=True).attrs['href']
        art_url = page.html.find('a[title=Download]', first=True).attrs['href']
        if (not url):
            raise ValueError("got empty url from the download server")

    except Exception as e:
        raise ValueError("Could not get download link from {0} - {1}".format(BASE_URL, str(e)))
    return (url, art_url)

def m3u_to_mp3(f):
    import requests, re

    z=open(f).read()
    con=list(map(lambda e: e.rstrip('\n').rstrip('#EXT-X-ENDLIST').rstrip("\n") if '#EXTM3U' not in e else '' ,re.split('#EXTINF:\d+.\d+,\n',z)))
    z = b''
    for r in con:
        if(r==''):continue;
        z+=requests.get(r).content
    open(f,'bw').write(z)

def set_mp3_tag(mp3_file, jpg_art_image, artist_name, track_name):
    import eyed3


    audiofile = eyed3.load(mp3_file)
    if audiofile == None:
        raise ValueError("MP3 file is invalid")

    audiofile.initTag((2, 3, 0))
    audiofile.tag.artist = artist_name
    audiofile.tag.title = track_name
    audiofile.tag.images.set(3, jpg_art_image, 'image/jpeg')
    audiofile.tag.save()