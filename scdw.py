import os
import utils

def load_playlist(playlist_url, chrome_driver_path, cache_folder):
    page_cache_file = cache_folder + 'page.html'

    if os.path.isfile(page_cache_file):
        with open(page_cache_file, "r",encoding="utf-8") as page_cache:
            page = page_cache.read()
    else:
        print("Requesting tracks list...")

        page = utils.download_tracks_page(chrome_driver_path, playlist_url)
        with open(page_cache_file, "w",encoding="utf-8") as page_cache:
            page_cache.write(page)

    print("Parsing tracks list...")
    return utils.parse_page(page)

def download_tracks(tracks, media_folder, report_folder):

    downloaded_tracks = list()
    already_downloaded_tracks = list()
    failed_tracks = list()

    local_tracks = list()

    i = 0
    print('Total {0} tracks.'.format(len(tracks)))
    for track in tracks:
        i = i + 1

        name = utils.fix_file_name(track.artist + " - " + track.track)
        track_file = media_folder + name + '.mp3'

        print("Processing track {0}/{1} {2} ".format(i, len(tracks), name))
        if os.path.isfile(track_file):
            local_tracks.append(utils.LocalTrack(track.artist, track.track, track.url, track_file))
            already_downloaded_tracks.append(track)
            print("Track is already downloaded.".format(i, len(tracks), name))
            continue

        try:
            print('Getting download link...', track.url)
            (url, art_url) = utils.request_track_art_urls(track.url)

            print('Downloading track file...', url)
            utils.download_to_file(url, track_file)

            try:
                with open(track_file, encoding='utf-8') as file:
                    is_m3u_playlist = True if ('#EXTM3U' in file.readline()) else False
            except Exception:
                is_m3u_playlist = False

            if is_m3u_playlist:
                print('M3u file detected. Downloading and converting to mp3...')
                utils.m3u_to_mp3(track_file)

            try:
                print('Downloading art image...', art_url)
                jpg_art_image = utils.download(art_url)
            except Exception:
                art_url = art_url.replace('original','t500x500')
                print(utils.bcolors.WARNING ,'Original art image is not available, downloading alternative image...', art_url, utils.bcolors.ENDC)
                jpg_art_image = utils.download(art_url)

            print('Checking file and updating MP3 tag...')
            utils.set_mp3_tag(track_file, jpg_art_image, track.artist, track.track)
        except Exception as e:
            failed_tracks.append(track)
            if os.path.isfile(track_file):
                os.remove(track_file)
            print(utils.bcolors.FAIL, "Error:", e, utils.bcolors.ENDC)
            print(utils.bcolors.FAIL, "Failed--------------------", utils.bcolors.ENDC)
            continue;

        downloaded_tracks.append(track)
        local_tracks.append(utils.LocalTrack(track.artist, track.track, track.url, track_file))
        print(utils.bcolors.OKGREEN, "Successed--------------------", utils.bcolors.ENDC)

    print("Already downloaded tracks:", len(already_downloaded_tracks))
    print("Downloaded tracks:", len(downloaded_tracks))
    print("Failed track downloads:", len(failed_tracks))

    failed_tracks_file = report_folder + 'failed_tracks.txt'

    with open(failed_tracks_file, "w", encoding="utf-8") as file:
        for track in failed_tracks:
            file.write('{0} - {1} - {2}\n'.format(track.artist, track.track, track.url))

    return local_tracks

def create_playlist(tracks, media_folder):
    with open(media_folder + "/playlist.m3u8", "w", encoding="utf-8") as file:
        for track in tracks:
            file.write(os.path.basename(track.file)+'\n');