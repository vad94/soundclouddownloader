
import os
import utils
import scdw

if __name__ == '__main__':
    chrome_driver_path = "C:/Program Files (x86)/ChromeDriver/chromedriver.exe"
    cache_report_folder = 'C:/workspace/soundclouddownloader/out/cache/'
    media_folder = 'C:/workspace/soundclouddownloader/out/media/'

    if not os.path.exists(media_folder):
        os.makedirs(media_folder)

    if not os.path.exists(cache_report_folder):
        os.makedirs(cache_report_folder)

    # t1 = utils.Track('rb','test','https://soundcloud.com/c2cdjs/03-rasco-bustin-20syl-remix')
    # t2 = utils.Track('cherokee-official','teenage-fantasy','https://soundcloud.com/cherokee-official/teenage-fantasy')
    # local_tracks =  scdw.download_tracks([t1, t2], media_folder, cache_report_folder)
    # scdw.create_playlist(local_tracks, media_folder)

    likes_url = 'https://soundcloud.com/vadim-burenkov/likes'
    tracks = scdw.load_playlist(likes_url, chrome_driver_path, cache_report_folder)
    local_tracks = scdw.download_tracks(tracks, media_folder, cache_report_folder)
    scdw.create_playlist(local_tracks, media_folder)